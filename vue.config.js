module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],

  pwa: {
    name: 'SimpleCrypt'
  },
  chainWebpack: config => {
    config.plugin("html").tap(args => {
      args[0].title = "SimpleCrypt";
      return args;
    });
  },

  runtimeCompiler: true
}