// import * as CryptoJS from "crypto-js";
import { SodiumPlus, CryptographyKey } from "sodium-plus";
/* eslint-disable */
//@ts-ignore
import { text2morse, morse2text } from "morse-pro/lib/morse-pro.js";
/* eslint-enable */

export interface B64EncBundle {
  content: string;
  buf: string;
  key: string;
}

export interface EncResult {
  data: string;
  password: string;
}
export let sodium: SodiumPlus;
// export const cjs = CryptoJS;
export async function initSodium() {
  if (!sodium) {
    sodium = await SodiumPlus.auto();
  }
  return sodium;
}

//#region VanillaJS
export function simToBase64(str: string) { return { data: btoa(str) } as EncResult; }
export function simFromBase64(str: string) { return { data: atob(str) } as EncResult; }
//#endregion
//#region Sodium
export async function getRandByteBuffer(size = 32) {
  return await sodium.randombytes_buf(size);
}

export async function getNewKey() {
  return await sodium.crypto_secretbox_keygen();
}

export async function generateGenericHash(
  msg: string,
  key = (null as unknown) as CryptographyKey,
  lenght = (undefined as unknown) as number
) {
  return await sodium.crypto_generichash(msg, key, lenght);
}

export async function encXCC1305(str: string, key: string) {
  const buf = await getRandByteBuffer(24);
  const k = await sodium.crypto_aead_xchacha20poly1305_ietf_keygen();
  // const k = CryptographyKey.from(key, "ascii");
  const enc = await sodium.crypto_aead_xchacha20poly1305_ietf_encrypt(
    str,
    buf,
    k,
    key
  );
  const b64 = enc.toString("base64");
  const data = {
    content: b64,
    buf: buf.toString("base64"),
    key: k.toString("base64"),
  } as B64EncBundle;
  const res = {
    data: btoa(JSON.stringify(data)),
    password: key,
  } as EncResult;
  return res;
}

export async function decXCC1305(str: string, key: string) {
  const enc = JSON.parse(atob(str)) as B64EncBundle;
  console.log(enc);
  const content = Buffer.from(enc.content, "base64");
  const buf = Buffer.from(enc.buf, "base64");
  const k = CryptographyKey.from(enc.key, "base64");
  const dec = await sodium.crypto_aead_xchacha20poly1305_ietf_decrypt(
    content,
    buf,
    k,
    key
  );
  // return {data: dec as string, password:null} as EncResult;
  console.log(dec.toString());
  return { data: dec.toString() } as EncResult;
}

export async function encSecretBox(str: string) {
  const buf = await getRandByteBuffer(24);
  const key = await sodium.crypto_secretbox_keygen();
  const enc = await sodium.crypto_secretbox(str, buf, key);
  const data = {
    content: enc.toString("base64"),
    buf: buf.toString("base64"),
  } as B64EncBundle;
  const res = {
    data: btoa(JSON.stringify(data)),
    password: key.toString("base64"),
  } as EncResult;
  return res;
}

export async function decSecretBpx(str: string, pass: string) {
  const enc = JSON.parse(atob(str)) as B64EncBundle;
  const dec = await sodium.crypto_secretbox_open(
    Buffer.from(enc.content, "base64"),
    Buffer.from(enc.buf, "base64"),
    CryptographyKey.from(pass, "base64")
  );
  return { data: dec.toString() } as EncResult;
}

//#endregion

// //#region CryptoJS
// export function StringToUTF8(str: string) {
//   return CryptoJS.enc.Utf8.parse(str);
// }
// export function UTF8ToString(wa: CryptoJS.lib.WordArray) {
//   return CryptoJS.enc.Utf8.stringify(wa);
// }
// export function toBase64(str: string) {
//   return CryptoJS.enc.Base64.stringify(StringToUTF8(str));
// }
// export function parsetBase64(str: string) {
//   return CryptoJS.enc.Base64.parse(str);
// }
// export function fromBase64(str: string) {
//   return UTF8ToString(parsetBase64(str));
// }
// export function encryptAES(str: string, pass: string) {
//   return CryptoJS.AES.encrypt(str, pass);
// }
// export function decryptAES(str: string, pass: string) {
//   return CryptoJS.AES.decrypt(str, pass);
// }
//#endregion

//#region Morse-Pro
export function toMorse(str: string): EncResult {
  return { data: text2morse(str, true).morse, password: '' };
}
export function morseToText(morse: string): EncResult {
  return { data: morse2text(morse, true).message, password: '' };
}
//#endregion
