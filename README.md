# SimpleCrypt
SimpleCrypt is a research project, with the intent to make encryption and other encoding methods, simple and easy for the average joe. It uses LibSodium for encryption.

Suggestions and Contributions are always welcomed.

The project is currently in early stages but methods already available are;
* Base64 (Not Encryption)
* Morse Code (Nicity)(Not Encryption)
* AEAD (XChaCha20-Poly1305)
* Shared-key authenticated encryption (Secret Box)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
